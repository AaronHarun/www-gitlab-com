---
layout: markdown_page
title: "Identity data"
---

#### GitLab Identity Data

Data below is as of 2018-04-04.

##### Country Specific Data

| Country Information                       | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 254   | 100%        |
| Based in the US                           | 138  |  54.33%      |
| Based in the UK                           | 17    | 6.69%      |
| Based in the Netherlands                  | 11    | 4.33%       |
| Based in Other Countries                  | 88    | 34.65%      |

##### Gender Data

| Gender (All)                              | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 254   | 100%        |
| Men                                       | 203   | 79.53%      |
| Women                                     | 51    | 20.08%      |
| Other Gender Options                      | 0     | 0%          |

| Gender in Leadership                      | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 26    | 100%        |
| Men in Leadership                         | 22    | 84.62%      |
| Women in Leadership                       | 4     | 15.38%      |
| Other Gender Options                      | 0     | 0%          |

| Gender in Development                     | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 109   | 100%        |
| Men in Development                        | 98    | 89.91%      |
| Women in Development                      | 11    | 10.09%      |
| Other Gender Options                      | 0     | 0%          |

##### Race/Ethnicity Data

| Race/Ethnicity (US Only)                  | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 138   | 100%        |
| Asian                                     | 10    | 7.25%       |
| Black or African American                 | 3     | 2.17%       |
| Hispanic or Latino                        | 7     | 5.07%       |
| Native Hawaiian or Other Pacific Islander | 1     | 0.72%       |
| Two or More Races                         | 4     | 2.90%       |
| White                                     | 76    | 55.07%      |
| Unreported                                | 37    | 26.81%      |

| Race/Ethnicity in Development   (US Only) | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 28    | 100%        |
| Asian                                     | 4     | 14.29%      |
| Black or African American                 | 1     | 3.57%       |
| Two or More Races                         | 1     | 3.57%       |
| White                                     | 16    | 57.14%      |
| Unreported                                | 6     | 21.43%      |

| Race/Ethnicity in Leadership (US Only)    | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 22    | 100%        |
| Asian                                     | 2     | 9.09%       |
| Native Hawaiian or Other Pacific Islander | 1     | 4.55%       |
| White                                     | 12    | 54.55%      |
| Unreported                                | 7     | 31.82%      |

| Race/Ethnicity (Global)                   | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 254   | 100%        |
| Asian                                     | 16    | 6.30%       |
| Black or African American                 | 6     | 2.36%       |
| Hispanic or Latino                        | 13    | 5.12%       |
| Native Hawaiian or Other Pacific Islander | 1     | 0.39%       |
| Two or More Races                         | 5     | 1.97%       |
| White                                     | 135   | 53.15%      |
| Unreported                                | 78    | 30.71%      |

| Race/Ethnicity in Development (Global)    | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 109   | 100%        |
| Asian                                     | 8     | 7.34%       |
| Black or African American                 | 3     | 2.75%       |
| Hispanic or Latino                        | 5     | 4.59%       |
| Two or More Races                         | 2     | 1.83%       |
| White                                     | 54    | 49.54%      |
| Unreported                                | 37    | 33.94%      |

| Race/Ethnicity in Leadership (Global)     | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 26    | 100%        |
| Asian                                     | 2     | 7.69%       |
| Native Hawaiian or Other Pacific Islander | 1     | 3.85%       |
| White                                     | 13    | 50.00%      |
| Unreported                                | 10    | 38.46%      |

##### Age Distribution

| Age Distribution (Global)                 | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 254   | 100%        |
| 20-24                                     | 15    | 5.91%       |
| 25-29                                     | 63    | 24.80%      |
| 30-34                                     | 67    | 26.38%      |
| 35-39                                     | 37    | 14.57%      |
| 40-49                                     | 45    | 17.72%      |
| 50-59                                     | 23    | 9.06%       |
| 60+                                       | 2     | 0.79%       |
| Unreported                                | 2     | 0.79%       |
